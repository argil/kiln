# Argil

## Why make **another** language?
Although compiled languages have experienced a recent revolutions inspired by Go and Rust, dynamic light-weight languages have languished, growing static. 
Argil exists to introduce a new way to write quick but powerful programs. Embed Argil in any language that interfaces with C *(WIP)*. Create decentralized, blockchain apps easily *(WIP)*, and write the web in a new language designed for flexability.

## Schema
Argil takes its inspiration from Rust Go, PHP, and an assortment of other languages. One of the goals of Argil is to have a short, easily recognizable & replicable syntax.

### Variables
Use ```let``` to declare a variable. Variables in Argil are **immutable** by default.
```argil
let name = "Tristan"
let birth_month = "July"
```
To make a variable mutable, prepend ```mut``` to the variable name.
```argil
let mut occupation = "Programmer"
let mut bed_time = 10
```

```argil
let vehicle = "car" // Immutable
let $color = "red"  // Mutable
```

### Comments
```argil
// This is a single line comment
/// This is a documentation comment
//The comment below this is an example comment. The code in the brackets will be represented in a code block in the markdown documentation.
//[
func complex function() {
    // blah blah blah...
}
]//
```

### Functions
```argil
func example (one: int, two: rune) rune {
    // Example code
}
```
Use the ```func``` keyword to initialize a function. 

Arguments are ```name: type```. 

Return a type by specifiying it after arguments. Argil **does not** support implicit returns, unlike in Rust. Use the ```return``` keyword.
```argil
func return_example (one: int) rune {
    return std::types::parse(one, <rune>)
}
```

# To Be Done
Most of what you've seen so far is concept. Argil is in very early alpha, and I've yet to decide the schema for objects, modules, and other concepts. Let alone the actual parsing, AST, and compilation to target.

**Your help would be greatly appreciated!!!**

Community creation in progress -- in the meantime [Telegram](https://t.me/TristanIsham).
