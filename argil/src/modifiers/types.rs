pub enum ArgilType {
    Rune,
    Int,
    Float,
    Boolean,
    Callable,
    Vector,
    Void
}

impl ArgilType {
    pub fn parse(arg: &str) -> Self {
        match arg {
            "rune" => Self::Rune,
            "int" => Self::Int,
            "float" => Self::Float,
            "bool" => Self::Boolean,
            "callable" => Self::Callable,
            "vector" => Self::Vector,
            _ => Self::Void
        }
    }
}