use std::collections::BTreeMap;
use crate::modifiers::{viz::FuncMods, types::ArgilType};

pub struct ArgilFunc {
    name: String,
    args: BTreeMap<String, ArgilType>,
    viz: FuncMods,
    returns: BTreeMap<ArgilType, String>,
    body: String
}

impl ArgilFunc {
    // pub fn eval(words: Vec<String>) -> Result<Option<Self>, String> {
    //     // Setting Visibility of function
    //     let mut viz = match FuncMods::parse(words[0]) {
    //         Some(v) => {
    //            v
    //         },
    //         None => {
    //             FuncMods::Local
    //         }
    //     };

    //     let mut func_placement = 0;

    //     // Finding Function Name
    //     let mut name = String::from("");        
    //     if words[0] == "func" {
    //         func_placement = 0;
    //         for chars in words[1].chars() {
    //             if chars.is_uppercase() {
    //                 return Err(format!("Char {} in {} cannot be capitalized", chars, words[1]));
    //             } else if chars.is_numeric() {
    //                 return Err(format!("Char {} in {} cannot be capitalized", chars, words[1]));
    //             } else if !chars.is_alphabetic() {
    //                 return Err(format!("Char {} in {} must be a supported letter", chars, words[1]));
    //             } else {
    //                 name = words[1];
    //             }
    //         }
    //     } else if words[1] == "func" {
    //         func_placement = 1;
    //         for chars in words[2].chars() {
    //             if chars.is_uppercase() {
    //                 return Err(format!("Char {} in {} cannot be capitalized", chars, words[2]));
    //             } else if chars.is_numeric() {
    //                 return Err(format!("Char {} in {} cannot be capitalized", chars, words[2]));
    //             } else if !chars.is_alphabetic() {
    //                 return Err(format!("Char {} in {} must be a supported letter", chars, words[2]));
    //             } else {
    //                 name = words[2];
    //             }
    //         }
    //     }

    //     //Finding Function Args
    //     let mut args = String::from("");
    //     for c in words[func_placement+1].chars() {
    //         let mut start = false;
    //         let mut end = false;
    //         if c == '(' {
    //             start = true;
    //         }

    //         if c == ')' {
    //             end = true
    //         }

    //         if c == '{' || c == '}' || (c as u8) == b'\n' || (c as u8) == b'\r' || (c as u8) == b'\t' {
    //             panic!(format!("Unclosed function {} {} {} {}", words[0], words[1], words[2], words[3]));
    //         }

    //         if start && !end {
    //             args.push(c);
    //         }
            
    //     }

    //     let arguments = Self::parse_args(args);
        
    //     todo!("finish parsing function lines!");
    //     // Returning Function
    //     Ok(Some(Self {
    //         viz: viz,
    //         name: name,
    //         args: arguments,

    //     }))
    // }
    // fn parse_args(args: String) {

    // }
}
