use crate::prelude::error::ArgilErr;

#[derive(Copy, Clone)]
pub enum Mutability {
    Immutable,
    Mutable,
}

pub struct Var {
    name: String,
    value: String,
    x: usize,
    y: usize,
    mutability: Mutability,
}

impl Var {
    pub fn new(name: String, value: String, x: usize, y: usize, mutability: Mutability) -> Self {
        Self {
            name,
            value,
            x,
            y,
            mutability,
        }
    }
    pub fn get_name(&self) -> &String {
        &self.name
    }
    pub fn value(&self) -> &str {
        &self.value
    }
    pub fn get_x(&self) -> usize {
        self.x
    }
    pub fn get_y(&self) -> usize {
        self.y
    }

    pub fn is_mut(&self) -> bool {
        match self.mutability {
            Mutability::Immutable => false,
            Mutability::Mutable => true,
        }
    }

    pub fn set_val(&mut self, val: String) {
        self.value = val;
    }

    pub fn parse(words: &Vec<String>, num: usize) -> String {
        let mut place = num;
        let mut result = String::from("");
        todo!("Need to finish parsing strings");
        while (place + 1) < words.len() {
            let mut word = words[place].clone();
            if !words[place].contains("\";") {
                word.remove_matches('"');
                result.push_str(&word);
            } else {
                word.remove_matches("\";");
                result.push_str(&word);
            }
            place += 1;
        }
        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_string_parse() {
        let target = vec![
            "\"this".to_string(),
            "is".to_string(),
            "a".to_string(),
            "test\";".to_string(),
        ];
        assert_eq!(Var::parse(&target, 0), "thisisatest");
    }
}
