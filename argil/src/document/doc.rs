use super::line::Line;
use crate::scope::dom::State;
use crate::scope::var::{Mutability, Var};
use std::collections::BTreeMap;
use std::fs::File;
use std::io::{self, prelude::*, BufReader};

pub struct Doc {
    lines: Vec<Line>,
    state_tree: Option<State>,
}

impl Doc {
    /// Initializes a new Doc object.
    pub fn new(infile: &str) -> Self {
        //Build vector of line types.
        let lines = match Self::get_all_lines(infile) {
            Ok(s) => s,
            Err(e) => panic!("Failed to build lines for document {}", e),
        };
        // Building File Object

        Self {
            lines,
            state_tree: None,
        }
    }
    /// Gets each line from the document, builds each into a Line object, and passes back to be built into the doc object.
    fn get_all_lines(infile: &str) -> io::Result<Vec<Line>> {
        let file = File::open(infile)?;
        let reader = BufReader::new(file);
        let mut i = 0;
        let mut lines: Vec<Line> = Vec::new();
        for line in reader.lines() {
            if let Ok(s) = line {
                let l = s;
                let words_str: Vec<&str> = l.split(" ").collect();
                let mut words: Vec<String> = Vec::new();
                for w in words_str {
                    words.push(w.to_string());
                }
                let new_line = Line::new(i, l.len(), words);
                lines.push(new_line);
            }
            i += 1;
        }
        Ok(lines)
    }
    /// The main runtime for running Argil scripts
    pub fn validate(&mut self) -> Result<(), &str> {
        if self.is_main() {
            self.load_modules();
            self.build_dom();
        }
        Ok(())
    }
    // Checks to see if the file is the *main* starting file for an Argil program.
    fn is_main(&self) -> bool {
        if &self.lines[0].body[0] == "#pragma" && &self.lines[0].body[1] == "main" {
            true
        } else {
            false
        }
    }

    fn load_modules(&self) {
        println!("Module Support Comming!");
    }
    /// Builds the virtual DOM for Argil.
    fn build_dom(&mut self) {
        let vars = self.get_vars();
        let state = State::new(0usize, Some(vars), None, None);
        self.state_tree = Some(state);
    }

    fn get_vars(&mut self) -> BTreeMap<String, Var> {
        let mut variables: BTreeMap<String, Var> = BTreeMap::new();
        for line in &self.lines {
            let mut word_num = 0;
            for word in &line.body {
                match word.as_str() {
                    "let" => match line.body[word_num + 1].as_str() {
                        "mut" => {
                            let name = line.body[word_num + 2].clone();
                            let x = 5usize;
                            let value = Var::parse(&line.body, (word_num as i128 - 1) as usize);
                            let y = line.num as usize;
                            let var = Var::new(name, value, x, y, Mutability::Mutable);
                            if let Ok(_) = Self::add_var(var, &mut variables) {}
                        }
                        _ => {
                            let name = line.body[word_num + 1].clone();
                            let x = 4usize;
                            let value = Var::parse(&line.body, (word_num as i128 - 1) as usize);
                            let y = line.num as usize;
                            let var = Var::new(name, value, x, y, Mutability::Immutable);
                            if let Ok(_) = Self::add_var(var, &mut variables) {}
                        }
                    },
                    "=" => {
                        let name = &line.body[word_num - 1];
                        let val = &Var::parse(&line.body, word_num + 1);
                        if let Some(x) = variables.get_mut(name) {
                            x.set_val(val.to_owned());
                        }
                    }
                    _ => {}
                }
                word_num += 1;
            }
            // let mut state = State::new(0, Some(variables), None, None);
        }

        for (name, data) in &variables {
            println!("{} : {}", name, &data.value())
        }

        variables
    }

    fn add_var(var: Var, tree: &mut BTreeMap<String, Var>) -> Result<(), String> {
        if let Some(x) = tree.get_mut(var.get_name()) {
            if var.is_mut() {
                *x = var;
            } else {
                eprintln!(
                    "Variable {} {}:{} is immutable. You cannot redefine it.",
                    &var.get_name().clone(),
                    &var.get_x(),
                    &var.get_y()
                );
            }
        } else {
            tree.entry(var.get_name().clone()).or_insert(var);
        }

        Ok(())
    }
}
